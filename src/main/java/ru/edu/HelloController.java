package ru.edu;

import java.util.Collection;

@Controller
// все запросы, начинающиеся с example, будут передаваться DispatcherServlet в данный контроллер
@RequestMapping(value = "/example")
public class HelloController {

    private UsersCatalog usersCatalog;

    @Autowired
    public void setUsersCatalog(UsersCatalog usersCatalog) {
        this.usersCatalog = usersCatalog;
    }

    @GetMapping("/info")
//  @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView info() {
        ModelAndView modelAndView = new ModelAndView();
        // задаем имя JSP страницы (отображение)
        modelAndView.setViewName("/hello.jsp");
        modelAndView.addObject("message", "Hello from SpringMVC!");
        return modelAndView;
    }

    // метод, показывающий список пользователей
    @GetMapping("/user/all")
    public ModelAndView allUsers() {
        Collection<UserInfo> users = usersCatalog.getAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/all_users.jsp");
        modelAndView.addObject("usersList", users);
        return modelAndView;
    }
}
