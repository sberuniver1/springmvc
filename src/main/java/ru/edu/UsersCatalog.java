package ru.edu;

import java.util.*;

@Component
public class UsersCatalog {
    private final Map<String, UserInfo> users = new HashMap<>();

    @PostConstruct
    public void init() {
        create(new UserInfo("1", "Anton"));
        create(new UserInfo("2", "Denis"));
    }

    public Collection<UserInfo> getAll() {
        return users.values();
    }

    public UserInfo create(UserInfo userInfo) {
        users.put(userInfo.getId(), userInfo);
        return userInfo;
    }

    public UserInfo update(UserInfo userInfo) {
        users.put(userInfo.getId(), userInfo);
        return userInfo;
    }

    public UserInfo deleteById(String userId) {
        return users.remove(userId);
    }

    public UserInfo getById(String userId) {
        return users.get(userId);
    }
}
