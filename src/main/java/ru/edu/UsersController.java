package ru.edu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = "/api/user", consumes = MediaType.ALL_VALUE)
public class UsersController {
    private UsersCatalog usersCatalog;

    @Autowired
    public void setUsersCatalog(UsersCatalog usersCatalog) {
        this.usersCatalog = usersCatalog;
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<UserInfo> getAll() {
        return usersCatalog.getAll();
    }

    // получение информации о пользователе по ID
    // формат запроса будет в таком виде: http://localhost:8080/api/user?userId=1
    @GetMapping
    public UserInfo getById(@RequestParam("userId") String id) {
        return usersCatalog.getById(id);
    }

    // создание пользователя
    @PostMapping("/create")
    public UserInfo createUser(@RequestBody UserInfo userInfo) {
        return usersCatalog.create(userInfo);
    }

    // обновление пользователя
    @PostMapping("/update")
    public UserInfo updateUser(@RequestBody UserInfo userInfo) {
        return usersCatalog.update(userInfo);
    }

    // удаление пользователя
    @DeleteMapping("/{userId}")
    public UserInfo deleteById(@@PathVariable("userId") String id) {
        return usersCatalog.deleteById(id);
    }
}
